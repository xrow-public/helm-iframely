# Iframely Helm Chart

An Iframely Helm Chart for Kubernetes.

This chart deploys a container running the [self-hosted version](https://github.com/itteco/iframely)
of [Iframely](https://iframely.com).

## Installation

```bash
helm upgrade --install iframely oci://registry.gitlab.com/xrow-public/helm-iframely/charts/iframely --version ${CI_DOCUMENTATION_LATEST_TAG}
```

Note: The chart version doesn`t match the application version. The bundled application
version matches the latest Iframely version.

## Documentation

Please also read the [Iframely documentation](https://iframely.com/docs).

Embed the Iframely Helm Chart as a dependency in your project's Helm Chart like this:
```yaml
dependencies:
  - name: iframely
    version: '${CI_DOCUMENTATION_LATEST_TAG}'
    repository: oci://registry.gitlab.com/xrow-public/helm-iframely/charts
```

The API endpoint will be exposed in the form `http://<release>-<iframely.fullname>:8061`
so if the release name is `example`, the endpoint will be: `http://example-iframely:8061`.

## Support

Enterprise support for this chart or Kubernetes is delivered by [XROW GmbH](https://www.xrow.de).
